---
---

# GTK and Guile

## About Guile

GNU Guile is an implementation of the Scheme programming language, supporting
the fifth and most of the sixth revision of the language report, as well as many
requests for implementation.

It contains a bytecode and JIT native code compiler and is designed as **the**
extension language for the GNU project.

## About G-Golf

G-Golf is a Guile object library for developing modern GNOME applications in Scheme.
It comprises a direct binding to the GObject Introspection API and higher-level functionality for importing
GNOME libraries and making GObject classes (and methods) available in Guile's object-oriented programming system, GOOPS.

## Hello world

```scheme
#! /bin/sh
# -*- mode: scheme; coding: utf-8 -*-
exec guile -e main -s "$0" "$@"
!#

;; This Hello World is fairly elaborate and highlights many features of G-Golf,
;; such as being able to require specific versions of modules or importing
;; classes by name.  It is taken directly from the G-Golf source tree.

(eval-when (expand load eval)
  (use-modules (oop goops))

  (default-duplicate-binding-handler
    '(merge-generics replace warn-override-core warn last))

  (use-modules (g-golf))

  (g-irepository-require "Gtk" #:version "4.0")
  (for-each (lambda (name)
              (gi-import-by-name "Gtk" name))
      '("Application"
        "ApplicationWindow"
        "Box"
        "Label"
        "Button")))


(define (activate app)
  (let ((window (make <gtk-application-window>
                  #:title "Hello"
                  #:default-width 320
                  #:default-height 240
                  #:application app))
        (box    (make <gtk-box>
                  #:margin-top 6
                  #:margin-start 6
                  #:margin-bottom 6
                  #:margin-end 6
                  #:orientation 'vertical))
        (label  (make <gtk-label>
                  #:label "Hello, World!"
                  #:hexpand #t
                  #:vexpand #t))
        (button (make <gtk-button>
                  #:label "Close")))

    (connect button
	     'clicked
	     (lambda (b)
               (close window)))

    (set-child window box)
    (append box label)
    (append box button)
    (show window)))


(define (main args)
  (let ((app (make <gtk-application>
               #:application-id "org.gtk.example")))
    (connect app 'activate activate)
    (let ((status (g-application-run app (length args) args)))
      (exit status))))
```

## See More

* GNU Guile: [https://www.gnu.org/software/guile/](https://www.gnu.org/software/guile/)
* Savannah Project Page: [http://savannah.gnu.org/projects/g-golf](http://savannah.gnu.org/projects/g-golf)
* G-Golf Website: [https://www.gnu.org/software/g-golf/](https://www.gnu.org/software/g-golf/)
* G-Golf Reference Manual: [https://www.gnu.org/software/g-golf/learn.html](https://www.gnu.org/software/g-golf/learn.html)
* Development (Git): [http://git.savannah.gnu.org/cgit/g-golf.git](http://git.savannah.gnu.org/cgit/g-golf.git)
